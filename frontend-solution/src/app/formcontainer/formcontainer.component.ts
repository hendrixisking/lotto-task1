import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalendarModule } from 'primeng/primeng';


@Component({
  selector: 'app-formcontainer',
  templateUrl: './formcontainer.component.html',
  styleUrls: ['./formcontainer.component.css']
})


export class FormcontainerComponent implements OnInit {

  myForm: FormGroup;
  formResult: string;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
	  this.myForm = this.fb.group({
		  firstname: ['Robert', Validators.required],
		  lastname: ['Koteles', Validators.required],
		  email: ['', [Validators.required, Validators.pattern('[a-z0-9.@]*')]],
		  birthday: ['', Validators.required]
	  });
  }


  onSubmit(form: FormGroup) {

	  if (!form.valid) {
		  this.formResult = '';	
	  } else {
		  this.formResult = 'Dear ' + form.value.firstname + ' ' + form.value.lastname + '(' + form.value.email  + '), thanks for your efforts.';
	  }
	  
	  
	  console.log('Valid?', form.valid); // true or false
	  console.log('Name', form.value.firstname + ' ' + form.value.lastname);
	  console.log('Email', form.value.email);
	  console.log('Birthday', form.value.birthday);
	  
  }

}
