
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  SharedModule,
  CalendarModule,
  AccordionModule,
  CarouselModule
} from 'primeng/primeng';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TestComponent } from './test/test.component';

import { FormcontainerComponent } from './formcontainer/formcontainer.component';
import { AccordionComponent } from './accordion/accordion.component';
import { CarouselComponent } from './carousel/carousel.component';

import { RestService } from './shared/service/rest.service';
import { FetchdataComponent } from './fetchdata/fetchdata.component';



@NgModule({

  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    TestComponent,
    AccordionComponent,
    CarouselComponent,
    FormcontainerComponent,
    FetchdataComponent
  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    AppRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AccordionModule,
    CalendarModule,
    CarouselModule
  ],

  providers: [
    RestService,
    { provide: LOCALE_ID, useValue: 'de-DE'}
  ],

  bootstrap: [ AppComponent ]
})

export class AppModule {}
