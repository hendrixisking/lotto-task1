import { Component, OnInit } from '@angular/core';
import { CarouselModule } from 'primeng/primeng';
import { FormcontainerComponent } from '../formcontainer/formcontainer.component';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})


export class CarouselComponent implements OnInit {


	items: MixedItems[];

	constructor() {
		this.items = [
			{ type: 'image', src: 'big_jimi-hendrix.jpg'},
			{ type: 'form'},
			{ type: 'getText', content: 'This is the GET text below:' },
		];
	}

	ngOnInit() {
	}

}


interface MixedItems {
	type?;
	src?;
	content?;
}