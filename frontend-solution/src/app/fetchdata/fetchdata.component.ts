import { Component, OnInit } from "@angular/core";
import { RestService } from '../shared/service/rest.service';
import { Observable } from 'rxjs/Observable';

@Component({
	selector: 'app-fetchdata',
	templateUrl: './fetchdata.component.html',
	styleUrls: ['./fetchdata.component.css']
})


export class FetchdataComponent implements OnInit {

	public activities$: Observable<any>;

	private urlString = 'http://jsonplaceholder.typicode.com/users'; //test for developers, use item.name
	//private urlString = 'http://localhost:8080/api/test'; //use item.value {"value":"Congrats. You've managed to do the api call!"}
	/*
	No 'Access-Control-Allow-Origin' header is present on the requested resource so fetching the data via GET works only from the same domain.
	Using the get() method it always try to call http://localhost:4200/:8080/api/test that's wrong. That is the reason why I defined a new method called getActivities().
	*/

	constructor(private activityService: RestService) { }

	ngOnInit() {
		this.activities$ = this.activityService.getActivities(this.urlString);
	}
}